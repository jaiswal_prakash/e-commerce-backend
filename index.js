const express = require('express');
var mongoose = require('mongoose');
const app = express();
const bodyParser = require('body-parser');



// -------connection for database-----------//

const mongoDB = 'mongodb://localhost/assignment1'; // create the mooc database 
mongoose.connect(mongoDB, {
    useNewUrlParser: true,
    useUnifiedTopology: true
}, () => {
    console.log('Mongodb connected successfully');
});

//----------------bodyParser for post---------------//


app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())

//  --------------routing----------------------- //

app.use('/product', require('./router/product-router'));
app.use('/category', require('./router/category-router'));
app.use('/user', require('./router/user-router'));
app.use('/cart', require('./router/cart-router'));
app.use('/order', require('./router/order-router'));


////----------------------------------google signup-----------------///

const passport = require('passport');
const cookieSession = require('cookie-session')
require("./passport-setup");


// For an actual app you should configure this with an experation time, better keys, proxy and secure
app.use(cookieSession({
    name: 'tuto-session',
    keys: ['key1', 'key2']
  }))

// Auth middleware that checks if the user is logged in
const isLoggedIn = (req, res, next) => {
    if (req.user) {
        next();
    } else {
        res.sendStatus(401);
    }
}

// Initializes passport and passport sessions
app.use(passport.initialize());
app.use(passport.session());

// Example protected and unprotected routes
app.get('/', (req, res) => res.send('Example Home page!'))
app.get('/failed', (req, res) => res.send('You Failed to log in!'))

// In this route you can see that if the user is logged in u can acess his info in: req.user
app.get('/good', isLoggedIn, (req, res) => res.send(`Welcome mr ${req.user.displayName}!`))

// Auth Routes
app.get('/google', passport.authenticate('google', { scope: ['profile', 'email'] }));

app.get('/google/callback', passport.authenticate('google', { failureRedirect: '/failed' }),
  function(req, res) {
    // Successful authentication, redirect home.
    res.redirect('/good');
  }
);

app.get('/logout', (req, res) => {

    req.session = null;
    req.logout();
    res.redirect('/');
   
});

// ----------- defining port number------------//

app.listen(3000, () => {
    console.log("listening to the port 3000");
});